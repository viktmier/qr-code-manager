<?php

namespace App\Controller;

use App\Service\QRCodeService;
use JMS\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class QRController extends AbstractApiController
{
    /**
     * @var SerializerInterface
     */
    protected $serializer;

    /**
     * @var QRCodeService
     */
    protected $QRCodeService;

    public function __construct(SerializerInterface $serializer, QRCodeService $QRCodeService)
    {
        parent::__construct($serializer);
        $this->QRCodeService = $QRCodeService;
    }

    public function createAction(Request $request): Response
    {
        $data = $this->transformJsonBody($request);

        if(empty($data->get('name'))) {
            return $this->prepareResponse('Name field is missing', Response::HTTP_BAD_REQUEST);
        }

        try {
            $response = $this->QRCodeService->createByName($data->get('name'));
        } catch (\Exception $exception) {
            return $this->prepareResponse(
                [
                    'status' => $exception->getCode(),
                    'errors' => $exception->getMessage(),
                ],
                400
            );
        }

        return $this->prepareResponse($response);
    }

    public function showAction(string $uniqueCode): Response
    {
        if(empty($uniqueCode)) {
            return $this->prepareResponse('Unique code iis missing', Response::HTTP_BAD_REQUEST);
        }
        try {
            $response = $this->QRCodeService->showSingleByUniqueCode($uniqueCode);
        } catch (\Exception $exception) {
            return $this->prepareResponse(
                [
                    'status' => $exception->getCode(),
                    'errors' => $exception->getMessage(),
                ],
                400
            );
        }

        return $this->prepareResponse($response);
    }

    public function deleteAction(string $uniqueCode): Response
    {
        if(empty($uniqueCode)) {
            return $this->prepareResponse('Unique code is missing', Response::HTTP_BAD_REQUEST);
        }
        try {
            $this->QRCodeService->deleteByUniqueCode($uniqueCode);
            $response = ['status' => 'Success'];
        } catch (\Exception $exception) {
            return $this->prepareResponse(
                [
                    'status' => $exception->getCode(),
                    'errors' => $exception->getMessage(),
                ],
                400
            );
        }

        return $this->prepareResponse($response);
    }
}
