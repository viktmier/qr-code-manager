<?php

namespace App\Repository;

use App\Entity\QRCode;

class QRCodeRepository extends AbstractRepository
{
    public function getEntityClass(): string
    {
        return QRCode::class;
    }

    public function findByUniqueCode(string $uniqueCode): QRCode
    {
        return $this
            ->createQueryBuilder('code')
            ->select()
            ->where('code.uniqueCode = :uniqueCode')
            ->setParameter('uniqueCode', $uniqueCode)
            ->getQuery()
            ->getSingleResult();
    }
}
