<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(
 *     name="qr_code",
 *     uniqueConstraints={
 *      @ORM\UniqueConstraint(columns={"unique_code"})
 * })
 * @ORM\Entity(repositoryClass="App\Repository\QRCodeRepository")
 */
class QRCode
{
    /**
     * @var int|null
     *
     * @ORM\Column(type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $uniqueCode;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * Getter of Id
     *
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Getter of UniqueCode
     *
     * @return string
     */
    public function getUniqueCode(): ?string
    {
        return $this->uniqueCode;
    }

    /**
     * Setter of UniqueCode
     *
     * @param string $uniqueCode
     *
     * @return static
     */
    public function setUniqueCode(string $uniqueCode): QRCode
    {
        $this->uniqueCode = $uniqueCode;

        return $this;
    }

    /**
     * Getter of Name
     *
     * @return string
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * Setter of Name
     *
     * @param string $name
     *
     * @return static
     */
    public function setName(string $name): QRCode
    {
        $this->name = $name;

        return $this;
    }
}
