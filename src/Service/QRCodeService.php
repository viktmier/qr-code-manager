<?php

namespace App\Service;

use App\Entity\QRCode;
use App\Repository\QRCodeRepository;

class QRCodeService
{
    const QR_GENERATOR = 'https://qrcode.tec-it.com/API/QRCode?data=';
    const EXCEPTION_NOT_FOUND = 'QR code with such code does not exist';

    /**
     * @var QRCodeRepository
     */
    protected $repository;

    public function __construct(QRCodeRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param string $name
     *
     * @return array
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function createByName(string $name): array
    {
        $qrCode = (new QRCode())
            ->setName($name)
            ->setUniqueCode(sha1($name. date('Y-m-d H:i:s')));
        $this->repository->save($qrCode);

        return $this->formResponseData(
            $qrCode->getUniqueCode(),
            $this->prepareQRDataString($qrCode)
        );
    }

    /**
     * @param string $uniqueCode
     *
     * @return array
     *
     * @throws \Exception
     */
    public function showSingleByUniqueCode(string $uniqueCode): array
    {
        try {
            $qrCode = $this->repository->findByUniqueCode($uniqueCode);
        } catch (\Exception $exception) {
            throw new \Exception(self::EXCEPTION_NOT_FOUND);
        }

        return $this->formResponseData(
            $qrCode->getUniqueCode(),
            $this->prepareQRDataString($qrCode)
        );
    }

    /**
     * @param string $uniqueCode
     *
     * @throws \Exception
     */
    public function deleteByUniqueCode(string $uniqueCode): void
    {
        try {
            $qrCode = $this->repository->findByUniqueCode($uniqueCode);
        } catch (\Exception $exception) {
            throw new \Exception(self::EXCEPTION_NOT_FOUND);
        }

        $this->repository->delete($qrCode);
    }

    protected function prepareQRDataString(QRCode $QRCode): string
    {
        return self::QR_GENERATOR .
            'Unique+code%3a+' . $QRCode->getUniqueCode() .
            '%0aName%3a+' .  preg_replace('/\s+/', '+', $QRCode->getName());
    }

    protected function formResponseData(string $uniqueCode, string $integrationLink): array
    {
        return [
            'uniqueCode'      => $uniqueCode,
            'integrationLink' => $integrationLink,
        ];
    }
}
