<?php

namespace App\Tests\Service;

use App\Repository\QRCodeRepository;
use App\Service\QRCodeService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Symfony\Bridge\PhpUnit\ClockMock;

class QRCodeServiceTest extends TestCase
{
    /**
     * @var MockObject|QRCodeRepository
     */
    protected $repository;

    /**
     * @var MockObject|QRCodeService
     */
    protected $qrService;

    public function testCreateByName(): void
    {
        ClockMock::register(QRCodeService::class);
        ClockMock::withClockMock(date_create('2020-01-01 20:22:23')->getTimestamp());

        $uniqueCode = sha1('Name of new QR code2020-01-01 20:22:23');
        $integrationLink = QRCodeService::QR_GENERATOR . 'Unique+code%3a+' . $uniqueCode . '%0aName%3a+Name+of+new+QR+code';
        $expectedResult = [
            'uniqueCode'      => $uniqueCode,
            'integrationLink' => $integrationLink,
        ];

        $actualResult = $this->qrService->createByName('Name of new QR code');
        $this->assertEquals($expectedResult, $actualResult);
        ClockMock::withClockMock(false);
    }

    public function testShowSingleByUniqueCodeException(): void
    {
        $this->repository->method('findByUniqueCode')->will(static::throwException(new \Exception()));;
        try {
            $this->qrService->showSingleByUniqueCode('');
        } catch (\Exception $exception) {
            $this->assertEquals(
                QRCodeService::EXCEPTION_NOT_FOUND,
                $exception->getMessage()
            );
        }
    }

    protected function setUp(): void
    {
        $this->repository = $this->mockCreator(QRCodeRepository::class, ['findByUniqueCode', 'save', 'delete']);

        $this->qrService = new QRCodeService($this->repository);
    }

    protected function mockCalls(MockObject $mock, array $calls): void
    {
        foreach ($calls as $key => $call) {
            $mock->expects($this->at($key))->method($call[0])->willReturn($call[1]);
        }
    }

    protected function mockCreator($class, array $methods = []): MockObject
    {
        $mock = $this->getMockBuilder($class)->disableOriginalConstructor();

        if (!empty($methods)) {
            $mock->setMethods($methods);
        }

        return $mock->getMock();
    }
}
