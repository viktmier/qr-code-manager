# QR code manager #

This manager generates AR code with your given "name" value.
##

### API endpoints: ###

##### POST:  /api/QR/create
* body example: 
`
{
	"name" : "Naujas QR kodas"
}
`

* successful response example: 
`{
    "uniqueCode": "c21bc20b79a3f1bfffcdad26cad0ed46966f01b7",
    "integrationLink": "https://qrcode.tec-it.com/API/QRCode?data=Unique+code%3a+c21bc20b79a3f1bfffcdad26cad0ed46966f01b7%0aName%3a+Naujas+QR+kodas"
}`

`"uniqueCode"` - this value is needed for further QR code manipulation (GET and DELETE)

##### GET:  /api/QR/show/`{uniqueCode}`
* Has same response as create

##### DELETE:  /api/QR/delete/`{uniqueCode}`
* successful response example: `{
                                    "status": "Success"
                                }`
#### Instructions for front-end applications:
* To display QR code in interface use `"integrationLink"` value in image tag `<img src="" />` 

example: `<img src="https://qrcode.tec-it.com/API/QRCode?data=Unique+code%3a+c21bc20b79a3f1bfffcdad26cad0ed46966f01b7%0aName%3a+Naujas+QR+kodas" />`





#
### Repository ###

* Mysql, version: 8
* Entities: QRCode

### How do I get set up? ###

* Service runs with docker, nginx, php, symfony, mysl.
* Repository is located: https://bitbucket.org/viktmier/qr-code-manager/src/master/  
